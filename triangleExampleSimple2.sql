﻿EXPLAIN (ANALYZE, COSTS, VERBOSE, BUFFERS, FORMAT JSON)
SELECT t1.source, t2.source, t3.source FROM e t1, e t2, e t3 
WHERE t1.target = t2.source
AND t2.target = t3.source
AND t3.target = t1.source;
