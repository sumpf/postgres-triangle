﻿--EXPLAIN (ANALYZE, COSTS, VERBOSE, BUFFERS, FORMAT JSON)
WITH vertexWithDegree AS (
	SELECT t1.source AS vertex, COUNT(t1.target) AS deg 
	FROM e t1 
	GROUP BY t1.source
), edgecountSqrt AS (
	SELECT SQRT(COUNT(*)) AS value FROM e
) SELECT * FROM (
		SELECT highDegVertex AS highDegVertex, t2.source AS opSource, t2.target AS opTarget FROM (
				SELECT vertexWithDegree.vertex AS highDegVertex FROM vertexWithDegree, edgecountSqrt WHERE
				vertexWithDegree.deg > edgecountSqrt.value
			) AS highDegVertex
			CROSS JOIN e AS t2
		WHERE 1 = CASE 
			WHEN (
				EXISTS (SELECT 1 FROM e AS t3 WHERE t3.target = t2.source AND t3.source = highDegVertex)
				AND EXISTS (SELECT 1 FROM e AS t3 WHERE t3.source = t2.target AND t3.target = highDegVertex)
			) THEN 1 ELSE 0 END
	UNION ALL	
		SELECT lowDegVertex, t2.target, t3.target FROM (
			SELECT vertexWithDegree.source AS lowDegVertex FROM
				(SELECT t1.source AS source, COUNT(t1.target) AS deg FROM e t1 GROUP BY t1.source)
					AS vertexWithDegree,
				(SELECT COUNT(*) AS count FROM e)
					AS edgecount
				WHERE 
					vertexWithDegree.deg <= SQRT(edgecount.count)
			) AS lowDegVertex,
			e AS t2,
			e AS t3
		WHERE t2.source = lowDegVertex
		AND t2.target = t3.source
		AND EXISTS (SELECT * FROM e AS t4 WHERE t4.source = t3.target AND t4.target = lowDegVertex)
) AS triangles