﻿EXPLAIN ANALYZE SELECT * FROM (
		SELECT highDegVertex, t2.source, t2.target FROM (
			SELECT vertexWithDegree.source AS highDegVertex FROM
				(SELECT t1.source AS source, COUNT(t1.target) AS deg FROM e t1 GROUP BY t1.source)
					AS vertexWithDegree,
				(SELECT COUNT(*) AS count FROM e)
					AS edgecount
				WHERE 
					vertexWithDegree.deg > SQRT(edgecount.count)
			) AS highDegVertex,
			e AS t2
		WHERE 
			EXISTS (SELECT * FROM e AS t3 WHERE t3.source = highDegVertex AND t3.target = t2.source)
			AND EXISTS (SELECT * FROM e AS t3 WHERE t3.target = highDegVertex AND t3.source = t2.target)
	UNION ALL
		SELECT lowDegVertex, t2.target, t3.target FROM (
			SELECT vertexWithDegree.source AS lowDegVertex FROM
				(SELECT t1.source AS source, COUNT(t1.target) AS deg FROM e t1 GROUP BY t1.source)
					AS vertexWithDegree,
				(SELECT COUNT(*) AS count FROM e)
					AS edgecount
				WHERE 
					vertexWithDegree.deg <= SQRT(edgecount.count)
			) AS lowDegVertex,
			e AS t2,
			e AS t3
		WHERE t2.source = lowDegVertex
		AND t2.target = t3.source
		AND EXISTS (SELECT * FROM e AS t4 WHERE t4.source = t3.target AND t4.target = lowDegVertex)
) AS triangles