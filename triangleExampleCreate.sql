﻿CREATE TABLE IF NOT EXISTS e (
    id serial NOT NULL PRIMARY KEY,
    source text NOT NULL,
    target text NOT NULL,
    UNIQUE (source, target)
); 