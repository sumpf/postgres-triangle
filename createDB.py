#!/usr/bin/env python3
import os,sys
import psycopg2

weakVertices = []
strongVertices = []
for i in range(3):
    strongVertices.append("s{}".format(str(i)))
for i in range(2000):
    weakVertices.append("w{}".format(str(i)))

try:
    conn = psycopg2.connect("dbname='triangles' user='postgres' host='localhost' password=''")
    print ("Connected to DB")
except:
    print ("I am unable to connect to the database")
    sys.exit(1)

cursor = conn.cursor()

# Create Database
createTable = """
CREATE TABLE IF NOT EXISTS e (
    id serial NOT NULL PRIMARY KEY,
    source text NOT NULL,
    target text NOT NULL,
    UNIQUE (source, target)
); 
"""
deleteAll = """TRUNCATE e;"""

try:
    cursor.execute(createTable)
    print("Created Table e")
    #print(cursor.fetchall())
except psycopg2.Error as e:
    print("Error Creating Table e: {0}".format(e.pgerror))
    sys.exit(1)

try:
    cursor.execute(deleteAll)
    print("Cleaned Table e")
except psycopg2.Error as e:
    print("Error Cleaning Table e: {0}".format(e.pgerror))
    sys.exit(1)
    
# Fill database
createStrongVertices = """
    INSERT INTO e (source,target) VALUES (%s, %s);
"""
for strong1 in strongVertices:
    for strong2 in strongVertices:    
        try:
            cursor.execute(createStrongVertices, (strong1, strong2))
            print("Created strong vertices")
        except psycopg2.Error as e:
            print("Error Creating strong vertices e: {0}".format(e.pgerror))

# Edges between strong and weak vertices
createWeakVertex = """
    INSERT INTO e (source,target) VALUES (%s,%s), (%s,%s);
"""
for weak in weakVertices:
    weakEdges = []
    createWeakVertex = ""
    for strong in strongVertices:
        try:
            cursor.execute(createWeakVertex, (weak, strong, strong, weak))
        except psycopg2.Error as e:
            print("Error Creating weak Vertex: {0}".format(e.pgerror))
print("created weak vertices")

conn.commit()
print("commited changes")
