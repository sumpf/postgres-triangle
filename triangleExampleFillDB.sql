﻿DO $$
DECLARE
  i int;
  r RECORD;
BEGIN
    DELETE FROM e;
    i := 0;
    INSERT INTO e (source, target) VALUES (
        'a','b'
    ), (
        'b','c'
    ), (
        'c','a'
    );

    WHILE i < 10000 LOOP
        INSERT INTO e (
            source,
            target
        ) VALUES (
            'x' || CAST (i AS text),
            'a'
        ), (
            'x' || CAST (i AS text),
            'b'
        ), (
            'x' || CAST (i AS text),
            'c'
        ), (
            'y' || CAST (i AS text),
            'a'
        ), (
            'y' || CAST (i AS text),
            'b'
        ), (
            'y' || CAST (i AS text),
            'c'
        ), (
            'z' || CAST (i AS text),
            'a'
        ), (
            'z' || CAST (i AS text),
            'b'
        ), (
            'z' || CAST (i AS text),
            'c'
        );
        i := i + 1;
    END LOOP;

    FOR r IN SELECT source, target FROM e LOOP
        INSERT INTO e (source, target) VALUES (r.target, r.source);
    END LOOP;
END
$$ LANGUAGE plpgsql; 